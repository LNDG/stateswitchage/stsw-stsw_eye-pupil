function d_phasic_1stDeriv_traces(group)

% plot traces for 1st derivative of pupil diameter

%% paths

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data    = fullfile(rootpath, 'data');
pn.statsOut = fullfile(rootpath, 'data', 'C_stats');
pn.figures = fullfile(rootpath, 'figures');
pn.tools   = fullfile(rootpath, 'tools');
    addpath(fullfile(pn.tools));
    addpath(genpath(fullfile(pn.tools, 'shadedErrorBar')));

%% load results

load(fullfile(pn.data, 'G1_summaryPupil.mat'), 'summaryPupil')
load(fullfile(pn.data, 'G1_pupilDataFT.mat'), 'pupilDataFT')

%% retrieve age labels

% restrict to age group
if strcmp(group, 'ya')
    idx_group = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))<2000;
elseif strcmp(group, 'oa')
    idx_group = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))>2000;
elseif strcmp(group, 'yaoa')
    idx_group = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))>0;
end

%% load statistical results for baselined data

load(fullfile(pn.statsOut, ['c_parametric_1stderiv_',group,'.mat']))

%% plot temporal traces with statistics

catAll = cat(4,pupilDataFT{2}.dataDerivSmooth, pupilDataFT{3}.dataDerivSmooth,...
        pupilDataFT{4}.dataDerivSmooth);

time = pupilDataFT{1}.time;
time = time(2:end)-3;

h = figure('units','normalized','position',[.1 .1 .25 .3]);
cla; hold on; 
% highlight different experimental phases in background
patches.timeVec = [3 6]-3;
patches.colorVec = [1 .95 .8];
for indP = 1:size(patches.timeVec,2)-1
    YLim = [-.25 1];
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end
condAvg = squeeze(nanmean(nanmean(catAll(idx_group,:,:,:),2),4))-...
    squeeze(nanmean(pupilDataFT{1}.dataDerivSmooth(idx_group,:,:),2));

curData = squeeze(nanmean(pupilDataFT{2}.dataDerivSmooth(idx_group,:,:),2))-...
    squeeze(nanmean(pupilDataFT{1}.dataDerivSmooth(idx_group,:,:),2));
curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(find(idx_group)),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', ...
    {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

curData = squeeze(nanmean(pupilDataFT{3}.dataDerivSmooth(idx_group,:,:),2))-...
    squeeze(nanmean(pupilDataFT{1}.dataDerivSmooth(idx_group,:,:),2));
curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(find(idx_group)),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', ...
    {'color', 4.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

curData = squeeze(nanmean(pupilDataFT{4}.dataDerivSmooth(idx_group,:,:),2))-...
    squeeze(nanmean(pupilDataFT{1}.dataDerivSmooth(idx_group,:,:),2));
curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(find(idx_group)),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', ...
    {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
legend([l2.mainLine, l3.mainLine, l4.mainLine], {'L2-L1'; 'L3-L1'; 'L4-L1'}, ...
    'orientation', 'horizontal', 'location', 'South'); legend('boxoff');

xlim([2.75 6.25]-3);  
time4minmax = find(pupilDataFT{1}.time>2.5 & pupilDataFT{1}.time<6);
minmax = [min(mean(condAvg(:,time4minmax),1)), ...
    max(mean(condAvg(:,time4minmax),1))];
ylim(minmax+[-0.5*diff(minmax), 0.5*diff(minmax)])
    
ylabel({'Pupil diameter change';'(1st derivative)'}); xlabel('Time (s from stim onset)')
title({'Pupil diameter during stimulus increases with load'; ''})
set(findall(gcf,'-property','FontSize'),'FontSize',18)

sigVals = double(stat.mask);
sigVals(sigVals==0) = NaN;
hold on; plot(time, sigVals.*minmax(1), 'k', 'linewidth', 5)

% plot as inset: overall pupil dilation

handaxes2 = axes('Position', [0.56 0.6 0.25 .25]);

    cla; hold on; 
    % highlight different experimental phases in background
    patches.timeVec = [3 6]-3;
    patches.colorVec = [.9 .9 .9];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-14 4];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    dataToPlot = squeeze(nanmean(nanmean(pupilDataFT{1}.dataDerivSmooth(idx_group,:,:),2),1));
    hold on; plot(time,dataToPlot, 'k', 'LineWidth', 2);
    ylabel({'Pupil diameter change';'(1st derivative)'}); xlabel('Time (s from stim onset)')
    title('Pupil diameter change: L1')
    xlim([2.75 6.25]-3); 
    time4minmax = find(pupilDataFT{1}.time>2.5 & pupilDataFT{1}.time<6);
    minmax = [min(dataToPlot(time4minmax)), ...
    max(dataToPlot(time4minmax))];
    ylim(minmax+[-0.3*diff(minmax), 0.3*diff(minmax)])

figureName = ['d_PupilDerivative_StimOnset_', group];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% summary pupil as median within cluster

% get the median only of significant time points

tmp_stimTime = find(pupilDataFT{1}.time>3 & pupilDataFT{1}.time<4.5);
%tmp_stimTime = tmp_stimTime(1) + find(sigVals(tmp_stimTime) == 1);
%summaryPupil = [];
for indCond = 1:4
    summaryPupil.stimPupilDeriv(:,indCond) = ...
        squeeze(nanmean(nanmedian(pupilDataFT{indCond}.dataDeriv(:,:,tmp_stimTime),3),2));
    summaryPupil.IDs = summaryPupil.IDs;
end

% calculate linear change in all measures

X = [1,1; 1,2; 1,3; 1,4];
b=X\summaryPupil.stimPupilDeriv'; % matrix implementation of multiple regression        
summaryPupil.stimPupilDeriv_slopes = b(2,:);

