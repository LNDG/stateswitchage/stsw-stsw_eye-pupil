%% assess linearity of effect

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = summaryPupil.stimPupilDeriv(1:47,i);
        data_ws{i, j} = summaryPupil.stimPupilDeriv(1:47,i)-...
            nanmean(summaryPupil.stimPupilDeriv(1:47,:),2)+...
            repmat(nanmean(nanmean(summaryPupil.stimPupilDeriv(1:47,:),2),1),size(summaryPupil.stimPupilDeriv(1:47,:),1),1); % individually demean: within-subject viz
    end
end

cl = [.6 .2 .2];

% make figure
h = figure('units','normalized','position',[.1 .1 .15 .2]); cla;
h_rc = rm_raincloud(data_ws, cl,1);
% add confidence intervals
hold on;
% for indCond = 1:4
%     line([h_rc.m(indCond,1).XData, h_rc.m(indCond,1).XData + errorY{indGroup}(1,indCond)],[h_rc.m(indCond,1).YData,h_rc.m(indCond,1).YData], 'Color', 'k', 'LineWidth', 5)
%     line([h_rc.m(indCond,1).XData, h_rc.m(indCond,1).XData - errorY{indGroup}(2,indCond)],[h_rc.m(indCond,1).YData,h_rc.m(indCond,1).YData], 'Color', 'k', 'LineWidth', 5)
% end

% add stats
% condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
% condPairsLevel = [-1.5 -1.4 -1.3 -1.2 -1.1, -1.0];
condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [-1.5 -1.4 -1.3];
for indPair = 1:size(condPairs,1)
    % significance star for the difference
    [~, pval] = ttest(data{condPairs(indPair,1)}, data{condPairs(indPair,2)}); % paired t-test
    % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
    % sigstars on top
    if pval <.05
       mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);
    end
end

view([90 -90]);
axis ij

set(gca, 'YTickLabels', flip({'1'; '2'; '3'; '4'})); % label assignment also has to be flipped
ylabel('Target load')
xlabel({'Pupil (1st derivative)';'[individually centered]'})
xlim([-4.3 -1]); yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./1.5, yticks(4)+(yticks(2)-yticks(1))./1.5]);

% test linear effect
X = [1 1; 1 2; 1 3; 1 4]; b=X\summaryPupil.stimPupilDeriv(1:47,:)'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);

title('Linear effect: p = 1.23e-6')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/A_pupilDilation/C_figures/B_pupilAtStim/';
figureName = 'C3_PupilDeriv_RainCloud_OA';

saveas(h, fullfile([pn.figures, figureName]), 'fig');
saveas(h, fullfile([pn.figures, figureName]), 'epsc');
saveas(h, fullfile([pn.figures, figureName]), 'png');
