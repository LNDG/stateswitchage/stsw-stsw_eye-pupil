====================
PLS SETTINGS README:
====================

Method: Type of PLS analysis.
    1 --> Mean-Centering Task PLS: Data-driven group / condition effects.

    2 --> Non-Rotated Task PLS: Hypothesis-driven group / condition effects.
            Must specify contrast-mat in (cond*group, lv) format.
            Contrast-mat: Matrix where each column is a contrast for non-rotated methods.

    3 --> Behavioral PLS: Data-driven group / condition effects of correlations between
            datamat and behavmat (Correlations calculated across subjects).
            Must specify behavmat in (subj*cond*group, lv) format (Already done by GUI).
            In GUI, simply select the .xls file that contains your behavior data.

    4 --> Multiblock PLS: Data-driven simultaneous group / condition effects in datamat
            and correlations with behavior. Must specify behavmat.
            In GUI, simply select the .xls file that contains your behavior data.

    5 --> Non-Rotated Behavioral PLS: Hypothesis-driven group / condition effects
            of correlations between datamat and behavmat (Correlations calculated across subjects).
            Must specify behavmat and contrastmat.
            Contrastmat is assumed to be in  (behav*cond*group, lv) format.

    6 --> Non-Rotated Multiblock PLS: Hypothesis-driven simultaneous group / condition effects
            in datamat and correlations with behavior.
            Must specify behavmat and contrastmat.
            For behavmat in GUI, simply select the .xls file that contains your behavior data.

            Each contrast is stacked as follows: Task effect followed by Behavior effect.
            Task effect is assumed to be in  (cond*group, lv) format.
            Behav effect is assumed to be in  (behav*bcond*group, lv) format.


Permutations: # of permutations for creating null distribution.
Bootstraps:   # of bootstrap samples.
Splits:       # of split samples.


Permutation Type (is_struct):
    0 --> Standard permutations of groups and conditions.

    1 --> Only permute subjects across groups and do not permute conditions within subjects.
            Think of example where conditions are vastly different
            (e.g structural MRI, cond1=white matter, cond2=gray matter)


Correlation Mode: Determines type of correlations to analyze.
    0 --> (default) Pearson correlation
    1 --> Pearson correlation which handles missing data (NaN's)
    2 --> covaraince no missing data
    3 --> covariance with missing data
    4 --> cosine angle no missing data
    5 --> cosine angle with missing data
    6 --> dot product no missing data
    7 --> dot product with missing data


Mean-Centering Type:
    Relevant for methods: Mean-centered, Non-rotated, Multiblock, and Non-rotated Multiblock.

    0 --> (Default) Within each group, remove group means from conditon means.
            Tells us how condition effects are modulated by group membership.
            Boost condition differences, remove overall group diffrences: (cond and cond by group).

    1 --> Remove grand condition means from each group condition mean.
            Tells us how conditions are modulated by group membership
            Boost group differences, remove overall condition diffrences: (group and group by cond).

    2 --> Remove grand mean (over all subjects and conditions).
            Tells us full spectrum of condition and group effects: (group, cond and group by cond).

    3 --> Remove all main effects, subtract cond and group means (group by condition).
            This type of analysis will deal with pure group by cond interaction.


Bootstrap Type: Bootstrap resampling type.
	1 --> (Default) Stratified (Old PLS)
    2 ---> Non-stratified.
