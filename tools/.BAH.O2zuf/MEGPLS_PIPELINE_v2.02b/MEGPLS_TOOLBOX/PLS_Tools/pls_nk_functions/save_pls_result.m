function save_pls_result(res,dim4d,roi,boot_thresh,clamp_range,Info,prefix)
  %%%% save bootstrap result as 4D brik file
  if numel(dim4d)~=4
    error('dim must be a vector of length 4');
  end
  num_times = dim4d(4);
  
  sig_lvs = reshape(find(res.perm_result.sprob < 0.1),1,[])
  
  for lv = sig_lvs
    bsr = single(res.boot_result.compare_u(:,lv));
    bsr(find(abs(bsr)<boot_thresh)) = 0;
    % also clamp for display purposes
    bsr(find(bsr > clamp_range(2))) = clamp_range(2);  
    bsr(find(bsr < clamp_range(1))) = clamp_range(1);
    bsr = reshape(bsr,[],dim4d(4));
    
    boot_img = zeros(dim4d); 
    boot_img(1,1,1,1) = -5;boot_img(1,1,1,2) = 5;
    for t=1:dim4d(4),
      tmp = reshape(squeeze(boot_img(:,:,:,t)),1,[]);
      tmp(roi) = bsr(:,t);     
      boot_img(:,:,:,t) = reshape(tmp,dim4d(1),dim4d(2),dim4d(3));
    end
    Info.RootName = [prefix '_LV' num2str(lv)];
    
    if ismember(res.method,[1,2])
        Info.HISTORY_NOTE = [Info.HISTORY_NOTE ' *** PLS: LV ' num2str(lv) ' p = ' num2str(res.perm_result.sprob(lv)) ' v = ' num2str(reshape(squeeze(res.v(:,lv)),1,[]))];
    elseif ismember(res.method,[3,5,4,6])
        Info.HISTORY_NOTE = [Info.HISTORY_NOTE ' *** PLS: LV ' num2str(lv) ' p = ' num2str(res.perm_result.sprob(lv)) ' corr = ' num2str(reshape(squeeze(res.boot_result.orig_corr(:,lv)),1,[])) ' llcorr = ' num2str(reshape(squeeze(res.boot_result.llcorr(:,lv)),1,[])) ' ulcorr = ' num2str(reshape(squeeze(res.boot_result.ulcorr(:,lv)),1,[]))];
    end
        
    clear Opt;
    Opt.scale = 0;
    Opt.view = '+tlrc';
    Opt.verbose = 1;
    Opt.AppendHistory = 1;
    Opt.NoCheck = 0;
    Opt.OverWrite = 'y';
    Opt.Prefix = Info.RootName;
    
    [err, ErrMessage, Info] =  WriteBrik(boot_img, Info, Opt);
    
    % depending on type of PLS save group avrages or correlations or both
    num_groups = numel(res.datamat_lst);
    num_conds = res.num_conditions;
    if ismember(res.method,[1,2,4,6])
        % save group average time courses
        for g=1:num_groups
            numsubs = res.num_subj_lst(g);
            data = reshape(res.datamat_lst{g},numsubs,num_conds,[],dim4d(4)); % (subj cond voxel time)
            data = squeeze(mean(data,1)); % (cond voxel time)
            for cond=1:num_conds
                container = zeros(dim4d); % (x y z time)
                for t=1:dim4d(4),
                    tmp = reshape(squeeze(container(:,:,:,t)),1,[]);
                    tmp(roi) = squeeze(data(cond,:,t));
                    container(:,:,:,t) = reshape(tmp,dim4d(1),dim4d(2),dim4d(3));
                end
                Info.RootName = [prefix '_group' num2str(g) '_cond' num2str(cond) '_avgts'];
                Info.HISTORY_NOTE = [Info.HISTORY_NOTE ' Group/Cond average time series '];
                clear Opt;
                Opt.scale = 0;
                Opt.view = '+tlrc';
                Opt.verbose = 1;
                Opt.AppendHistory = 1;
                Opt.NoCheck = 0;
                Opt.OverWrite = 'y';
                Opt.Prefix = Info.RootName;
                [err, ErrMessage, Info] =  WriteBrik(container, Info, Opt);
            end % for cond
        end % for g
    end
    if ismember(res.method,[3,5,4,6])
        % save group/condition/behavior specific correlation maps
        num_behavs = size(res.datamatcorrs_lst{1},1)/num_conds;
        for g=1:num_groups
            data = reshape(res.datamatcorrs_lst{g},num_behavs,num_conds,[],dim4d(4)); % (behav cond voxel time)
            for cond=1:num_conds
                for behav=1:num_behavs
                    
                    container = zeros(dim4d);
                    for t=1:dim4d(4),
                        tmp = reshape(squeeze(container(:,:,:,t)),1,[]);
                        tmp(roi) = squeeze(data(behav,cond,:,t));
                        container(:,:,:,t) = reshape(tmp,dim4d(1),dim4d(2),dim4d(3));
                    end
                    Info.RootName = [prefix '_group' num2str(g) '_cond' num2str(cond) '_behav' num2str(behav) '_corrts'];
                    Info.HISTORY_NOTE = [Info.HISTORY_NOTE ' Group/Cond/Behav correlation time series '];
                    clear Opt;
                    Opt.scale = 0;
                    Opt.view = '+tlrc';
                    Opt.verbose = 1;
                    Opt.AppendHistory = 1;
                    Opt.NoCheck = 0;
                    Opt.OverWrite = 'y';
                    Opt.Prefix = Info.RootName;
                    [err, ErrMessage, Info] =  WriteBrik(container, Info, Opt);
                end % for behav
            end % for cond
        end % for g
    end
    
  end
