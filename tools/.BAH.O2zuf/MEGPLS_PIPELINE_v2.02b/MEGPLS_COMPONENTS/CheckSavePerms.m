%=================================================================%
% Checks status & permissions of specified directory / file.      %
% Similar to CheckSavePath except existing files are NOT removed. %
%                                                                 %
% Note: This is for cases where "cfg.outputfile" is used.         %
% Checks output directory without removing any existing files.    %
% Last modified: Sept. 7, 2014                                    %
%=================================================================%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function CheckSavePerms(OutputPath, ErrLogName)
SaveErrLog = fopen(['ErrorLog_',ErrLogName,'.txt'], 'a');


% Check if directory or file:
if isdir(OutputPath)
    OutFolder = OutputPath;
else
    [OutFolder, ~, ~] = fileparts(OutputPath);
end

% Ensure directory exists:
if ~exist(OutFolder, 'dir')
    [Status, ErrMsg] = mkdir(OutFolder);
    
    if Status == 0
        ErrMsg = sprintf(['ERROR: Failed to create output directory:'...
            '\n Reason: %s \n Folder: %s \n\n'], ErrMsg, OutFolder);
        
        fprintf(SaveErrLog, ErrMsg);  % Record in ErrorLog
        error(ErrMsg);
    end
end

% Check write permissions on directory:
[~, DirAttrib] = fileattrib(OutFolder);

if DirAttrib.UserWrite ~= 1
    ErrMsg = sprintf(['ERROR: Do not have write permissions for output directory:'...
        '\n %s \n\n'], OutFolder);
    
    fprintf(SaveErrLog, ErrMsg);  % Record in ErrorLog
    error(ErrMsg);
end

% If file specified and exists, check permissions on overwrite:
if ~isdir(OutputPath) && exist(OutputPath, 'file')
    [~, FileAttributes] = fileattrib(OutputPath);
    
    if FileAttributes.UserWrite ~= 1
        ErrMsg = sprintf(['ERROR: Do not have write permissions to overwrite files:'...
            '\n %s \n\n'], OutputPath);
        
        fprintf(SaveErrLog, ErrMsg);  % Record in ErrorLog
        error(ErrMsg);
    end
end

fclose(SaveErrLog);
