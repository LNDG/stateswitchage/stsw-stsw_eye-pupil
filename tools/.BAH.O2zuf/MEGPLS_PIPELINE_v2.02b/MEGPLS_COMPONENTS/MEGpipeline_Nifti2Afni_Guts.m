%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Converts NIFTI files to AFNI format. %
% Last modified: Jan. 15, 2014         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Usage:
%  MEGpipeline_Nifti2Afni_Guts(PipeSettings, InputNifti, OutpathAfni)
%
% Inputs:
%  Note: PipeSettings can be generated & imported from a Builder .mat file.
%  PipeSettings.Afni4D.NaNFix   = 'yes' or 'no' (Fix NaNs to zeroes).
%  PipeSettings.Afni4D.Template = 'mni', 'tlrc', or 'orig' (Template of input file).
%
%  InputNifti  = Path to input NIFTI file to convert into AFNI.
%  OutpathAFNI = Output /path/filename of AFNI file.


% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_Nifti2Afni_Guts(PipeSettings, InputNifti4D, OutpathAfni)


% Check number of input arguments:
if nargin ~= 3
    error('ERROR: Incorrect number of input arguments. See help for usage info.')
end


% Convert to AFNI:
if exist(OutpathAfni, 'file')
    [AfniFolder, AfniFile, ~] = fileparts(OutpathAfni);
    delete([AfniFolder,'/',AfniFile,'.BRIK']);
    delete([AfniFolder,'/',AfniFile,'.HEAD']);
end
CheckSavePath(OutpathAfni, 'Nifti2Afni');

disp('Convert NIFTI into AFNI:')
system(['3dcopy ',InputNifti4D,' ',OutpathAfni]);


% From SPM, voxels outside mask / where sampling not acquired assigned NaNs.
% AFNI cannot read these values (Convert them to 0's).
if strcmp(PipeSettings.Afni4D.NaNFix, 'yes')
    disp('Converting NaNs to zeroes:')
    
    [Folder, File, Ext] = fileparts(OutpathAfni);
    TempFile = [Folder,'/Temp_',File,Ext];
    
    system(['float_scan -fix ',OutpathAfni,' > ',TempFile]);
    delete(OutpathAfni);
    Status = movefile(TempFile, OutpathAfni, 'f');
    
    if Status == 0
        disp('ERROR: Failed to replace old AFNI file with AFNI NaN-fixed file.')
        return;
    end
end


% Changes view-label to "+tlrc" (Unlocks coordinate & atlas functions in AFNI).
% *Warning*: This does NOT mean the template-space of the dataset is in "tlrc".
% *Warning*: Files in MNI-space will still have "+tlrc" tag.
switch PipeSettings.Afni4D.Template
    case 'mni'
        system(['3drefit -view tlrc -space MNI -newid -redo_bstat ',OutpathAfni]);
        
    case 'tlrc'
        system(['3drefit -view tlrc -space TLRC -newid -redo_bstat ',OutpathAfni]);
        
    case 'orig'
        system(['3drefit -view orig -space ORIG -newid -redo_bstat ',OutpathAfni]);
end

