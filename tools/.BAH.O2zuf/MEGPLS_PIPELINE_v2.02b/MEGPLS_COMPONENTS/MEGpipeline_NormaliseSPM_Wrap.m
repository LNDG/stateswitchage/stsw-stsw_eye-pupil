%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Normalises anatomical & functional volumes: %
% Last modified: Jan. 15, 2014                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_NormaliseSPM_Wrap(BuilderMat)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder		 = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
time         = Builder.time;
FTcfg        = Builder.FTcfg;
SPMcfg       = Builder.SPMcfg;
PipeSettings = Builder.PipeSettings;


% Clear existing Errorlog & Diary:
if exist('ErrorLog_NormaliseSPM.txt', 'file')
    system('rm ErrorLog_NormaliseSPM.txt');
end
if exist('Diary_NormaliseSPM.txt', 'file')
    system('rm Diary_NormaliseSPM.txt');
end

diary Diary_NormaliseSPM.txt
ErrLog = fopen('ErrorLog_NormaliseSPM.txt', 'a');



%================================%
% NORMALISE MRI & SOURCE IMAGES: %
%================================%

for g = 1:length(name.GroupID)
    NumSubj = length(name.SubjID{g});
    
    if UseProgBar == 1
        ppm = ParforProgMon(['NORMALISING SOURCE IMAGES:  ',name.GroupID{g},'.  '], NumSubj, 1, 700, 80);
    end
    
	for s = 1:length(name.SubjID{g})
        
		
        % Get source resolution & bounding box for subject:
        SourceSample = LoadFTmat(paths.Source{g}{s,1,1}, 'NormaliseSPM');
        if isempty(SourceSample)
            continue;
        else
            SourceBB     = SourceSample.cfg.grid;
            SourceRes    = SourceBB.cfg.grid.resolution;  % Get res in mm.
            SourceSample = [];  % Free memory
        end
        
        % Get input MRI data:
        MRIdata = paths.MRIdata{g}{s};
        
        % If specified, reslice & interp MRI to bounding-box of source images:
        if strcmp(PipeSettings.NormSPM.ResliceMri2Src, 'yes')
            MRIdata = LoadFTmat(paths.MRIdata{g}{s}, 'NormaliseSPM');
            if isempty(MRIdata)
                continue;
            end
            
            MRIdata = ft_checkdata(MRIdata, 'datatype', 'volume', 'feedback', 'yes');
            MRIdata = ft_convert_units(MRIdata, 'mm');
            
            cfgReslice            = [];
            cfgReslice.resolution = SourceRes;  % Get res and BB in mm.
            cfgReslice.xrange     = [SourceBB.xgrid(1), SourceBB.xgrid(end)];
            cfgReslice.yrange     = [SourceBB.ygrid(1), SourceBB.ygrid(end)];
            cfgReslice.zrange     = [SourceBB.zgrid(1), SourceBB.zgrid(end)];
            
            disp('Reslicing MRI to source:')
            MRIdata = ft_volumereslice(cfgReslice, MRIdata)
        end
        
        SourceBB = [];  % Free memory
        
        
        % Normalise MRI and get norm params:
        [~, NormParams] = MEGpipeline_NormMRI_Guts...
            (FTcfg, SPMcfg, MRIdata, SourceRes, paths.NormMRI{g}{s})
        
        if isempty(NormParams)
            disp('ERROR: Failed to compute normalisation parameters for subject.')
            continue;
        end
		
		
		%--- Normalise functional MEG images: ---%
		%----------------------------------------%
		
		for c = 1:length(name.CondID)
			parfor t = 1:size(time.Windows, 1)
				
				NormSource = MEGpipeline_NormSource_Guts...
                    (FTcfg, SPMcfg, paths.Source{g}{s,c,t}, ...
                    MRIdata, NormParams, SourceRes, paths.NormSource{g}{s,c,t})
                
                NormSource = [];  % Free memory
				
			end  % Time
		end  % Cond
		
        MRIdata    = [];  % Free memory
        NormParams = [];
        SourceRes  = [];
        
        if UseProgBar == 1 && mod(s, 1) == 0
            ppm.increment();  % move up progress bar
        end
		
	end  % Subj
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group


	
%=========================%
% CHECK FOR OUTPUT FILES: %
%=========================%

for g = 1:length(name.GroupID)
	for s = 1:length(name.SubjID{g})
		
		% Check for NormMRI .mat files:		
		if ~exist(paths.NormMRI{g}{s}, 'file')
			fprintf(ErrLog, ['ERROR: Output NormMRI file(s) missing:'...
				'\n %s \n\n'], paths.NormMRI{g}{s});
		end
		
		% Check for NormSource .mat files:
		for c = 1:length(name.CondID)
			MissingFiles = 0;
			
			for t = 1:size(time.Windows, 1)
				if ~exist(paths.NormSource{g}{s,c,t}, 'file')
					if MissingFiles == 0
						MissingFiles = 1;
						
						fprintf(ErrLog, ['\nERROR: Output NormSource file(s) missing:'...
							'\n %s \n'], Folder);
					end
					
					fprintf(ErrLog, ' - Time: %s \n', File);
				end
				
			end  % Time
		end  % Cond
		
	end  % Subj
end  % Group



%=================%

if exist([pwd,'/ErrorLog_NormaliseSPM.txt'], 'file')
    LogCheck = dir('ErrorLog_NormaliseSPM.txt');
    if LogCheck.bytes ~=0  % File not empty
        open('ErrorLog_NormaliseSPM.txt');
    else
        delete('ErrorLog_NormaliseSPM.txt');
    end
end

fclose(ErrLog);
diary off
				
