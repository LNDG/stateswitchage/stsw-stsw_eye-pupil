%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrapper for Headmodel & Leadfield generation:     %
% Inputs: Segmented MRI & Preprocessed MEG data.    %
% Last modified: Jan. 15, 2014                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_HdmLeadfield(BuilderMat)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder		 = load(BuilderMat);
name		 = Builder.name;
paths		 = Builder.paths;
FTcfg        = Builder.FTcfg;


% Clear existing Errorlog & Diary:
RunSection = 'HdmLeadfield';

if exist(['ErrorLog_',RunSection,'.txt'], 'file')
    delete(['ErrorLog_',RunSection,'.txt']);
end
if exist(['Diary_',RunSection,'.txt'], 'file')
    delete(['Diary_',RunSection,'.txt']);
end

diary(['Diary_',RunSection,'.txt'])
ErrLog = fopen(['ErrorLog_',RunSection,'.txt'], 'a');



%==================================%
% GENERATE HEADMODELS & LEADFIELD: %
%==================================%

for g = 1:length(name.GroupID)
    NumSubj = length(name.SubjID{g});
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['HEADMODELS & LEADFIELDS:  ',name.GroupID{g},'.  '], NumSubj, 1, 700, 80);
    end
    
	parfor s = 1:length(name.SubjID{g})
		for c = 1:length(name.CondID)
			
            % Load input data (no cfg.inputfile options available):
			SegMRI  = LoadFTmat(paths.SegMRI{g}{s},    'HdmLeadfield');
			MEGdata = LoadFTmat(paths.MEGdata{g}{s,c}, 'HdmLeadfield');
            if isempty(SegMRI) || isempty(MEGdata)
                continue;
            end
            
            
            % Generate headmodel:
            cfgHdm      = [];
            cfgHdm      = FTcfg.Hdm;
            cfgHdm.grad = MEGdata.grad;  % Only required for localspheres
            
            disp('Generating Headmodel:')
            Headmodel = ft_prepare_headmodel(cfgHdm, SegMRI)
            
            CheckSavePath(paths.Hdm{g}{s,c}, 'HdmLeadfield');
            ParforSaveHdm(paths.Hdm{g}{s,c}, Headmodel)
            
            SegMRI = [];  % Free memory
            
            
            % Compute leadfield:
            % Note: Do not use cfg.inputfile here, sometimes grad info cannot be found.
            cfgLead     = [];
            cfgLead	    = FTcfg.Lead;
            cfgLead.vol = Headmodel;
            
            Headmodel = [];  % Free memory
            
            disp('Computing Leadfield:')
            Leadfield = ft_prepare_leadfield(cfgLead, MEGdata)
            
            CheckSavePath(paths.Lead{g}{s,c}, 'HdmLeadfield');
            ParforSaveLead(paths.Lead{g}{s,c}, Leadfield)
            
            Leadfield = [];  % Free memory
            MEGdata   = [];
			
		end  % Cond
        
        if UseProgBar == 1 && mod(s, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
	end  % Subj
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group



%=========================%
% CHECK FOR OUTPUT FILES: %
%=========================%

for g = 1:length(name.GroupID)
	for s = 1:length(name.SubjID{g})
		for c = 1:length(name.CondID)
            
			if ~exist(paths.Hdm{g}{s,c}, 'file')
				fprintf(ErrLog, ['ERROR: Output headmodel file missing:'...
					'\n %s \n\n'], paths.Hdm{g}{s});
            end
            
			if ~exist(paths.Lead{g}{s,c}, 'file')
				fprintf(ErrLog, ['ERROR: Output leadfield file missing:'...
					'\n %s \n\n'], paths.Lead{g}{s});
            end
            
		end  % Cond
	end  % Subj
end  % Group



%=================%

if exist([pwd,'/ErrorLog_HdmLeadfield.txt'], 'file')
    LogCheck = dir('ErrorLog_HdmLeadfield.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_HdmLeadfield.txt');
    else
        delete('ErrorLog_HdmLeadfield.txt');
    end
end

fclose(ErrLog);
diary off

end



function ParforSaveHdm(OutputPath, Headmodel)
    Headmodel
    save(OutputPath, 'Headmodel');
end

function ParforSaveLead(OutputPath, Leadfield)
    Leadfield
    save(OutputPath, 'Leadfield');
end
