%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Normalises source and volumes via Fieldtrip and SPM. %
% Last modified: Jan. 15, 2014                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Usage:
%  NormSource = MEGpipeline_NormSource_Guts...
%	(FTcfg, SPMcfg, SourceData, MRIdata, NormParams, Res, OutpathNormSource)
%
% Inputs:
%  Note: FTcfg & SPMcfg can be generated & imported from a Builder .mat file.
%  FTcfg.InterpSrc    = Fieldtrip config for ft_sourceinterpolate.
%  FTcfg.WriteNormSrc = Fieldtrip config for ft_volumewrite.
%
%  SPMcfg.Estimate = SPM config estimate field from spm_get_defaults.
%  SPMcfg.Write    = SPM config write field from spm_get_defaults.
%
%  SourceData        = Loaded FieldTrip structure containing SourceData OR path to FT .mat file.
%  MRIdata           = Loaded FieldTrip structure containing MRIdata    OR path to FT .mat file.
%
%  NormParams        = Normalisation parameters from MEGpipeline_NormMRI_Guts.
%  Res               = Desired resolution (in mm) of output normalised source.
%  OutpathNormSource = Output /path/filename of normalised source.


% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function NormSource = MEGpipeline_NormSource_Guts...
	(MainFTcfg, MainSPMcfg, SourceData, MRIdata, NormParams, Res, OutpathNormSource)


NormSource = [];  % Initialize output variable


% Check number of input & output arguments:
if nargin ~= 7
    error('ERROR: Incorrect number of input arguments. See help for usage info.')
end
if nargout > 1
    error('ERROR: Incorrect number of output arguments. See help for usage info.')
end


% If inputs are paths to .mat files, load & check them:
if ~isstruct(SourceData)
    SourceData = LoadFTmat(SourceData, 'NormaliseSPM');
    if isempty(SourceData)
        return;
    end
end
if ~isstruct(MRIdata)
    MRIdata = LoadFTmat(MRIdata, 'NormaliseSPM');
    if isempty(MRIdata)
        return;
    end
end


% Load settings:
SPMcfg.Estimate  = MainSPMcfg.Estimate;
SPMcfg.Write     = MainSPMcfg.Write;
SPMcfg.Write.vox = [Res, Res, Res];


% Interpolate source data to anatomy:
FTcfg.InterpSrc = [];
FTcfg.InterpSrc = MainFTcfg.InterpSrc;

if isempty(FTcfg.InterpSrc.parameter)
    InterpFields = parameterselection('all', SourceData);  % Get volume fields to interpolate.
    InterpFields(strcmp(InterpFields, 'pos')) = [];        % Don't interpolate 'pos' field.
    FTcfg.InterpSrc.parameter = InterpFields;
end

disp('Interpolating source image to MRI:')
Interp = ft_sourceinterpolate(FTcfg.InterpSrc, SourceData, MRIdata)

SourceData = [];  % Free memory
MRIdata    = [];



%=============================%
% BEGIN SOURCE NORMALISATION: %
%=============================%

% Convert interpolated data into approximate RAS-orientation (SPM space):
% Keep initial transformation matrix (To emulate "ft_volumenormalise").
Interp = ft_checkdata(Interp, 'datatype', 'volume', 'feedback', 'yes');
Interp = ft_convert_units(Interp, 'mm');

orig    = Interp.transform;
Interp  = ft_convert_coordsys(Interp, 'spm');
initial = Interp.transform / orig;


% Get volume fields to normalise:
disp('Writing source volume fields to NIFTI for SPM:')
VolumeFields = parameterselection('all', Interp);  % Get volume fields

FindAnat = strcmp(VolumeFields, 'anatomy');  % from "ft_volumenormalise"
if ~any(FindAnat)
    VolumeFields = [{'anatomy'}, VolumeFields];
else
    [~, Index]   = sort(FindAnat);
    VolumeFields = VolumeFields(fliplr(Index));
end


% Normalise with SPM and read back to FT:
[FolderNormSource, NameNormSource, ~] = fileparts(OutpathNormSource);

NormSource = [];
for f = 1:length(VolumeFields)
	
	% Write volume to NIFTI:
    TempFilename = [VolumeFields{f},'_',NameNormSource];
    TempFilename(strfind(TempFilename, '.')) = [];  % ft_volumewrite cannot read dots.
    
	FTcfg.WriteNormSrc			 = [];
	FTcfg.WriteNormSrc			 = MainFTcfg.WriteNormSrc;
	FTcfg.WriteNormSrc.parameter = VolumeFields{f};
	FTcfg.WriteNormSrc.filename  = TempFilename;
	FTcfg.WriteNormSrc.filetype  = 'nifti';
    
	ft_volumewrite(FTcfg.WriteNormSrc, Interp)
	
	% Normalise volume and read back into FT:
	TempFile = [TempFilename,'.nii'];
	spm_write_sn(TempFile, NormParams, SPMcfg.Write);
	
	hdr        = spm_vol_nifti(['w',TempFile]);
	img        = spm_read_vols(hdr);
	NormSource = setsubfield(NormSource, VolumeFields{f}, img);
    
    system(['rm ',TempFile]);   % Remove remnant volume field files.
	system(['rm w',TempFile]);	
    
    % Keep anatomy header:
    if strcmp(VolumeFields{f}, 'anatomy')
        AnatHdr = hdr;
    end
    
    hdr = [];  % Free memory
    img = [];
end

if isfield(NormSource, 'inside')  % Convert inside back to logical
    NormSource.inside  = abs(NormSource.inside-1)<=10*eps;
end

NormSource.dim		 = size(NormSource.anatomy);
NormSource.transform = AnatHdr.mat;
NormSource.params	 = NormParams;
NormSource.initial	 = initial;
NormSource.coordsys  = 'spm';
NormSource.unit		 = 'mm';


% Align volume (voxel-space) with headcoordinate axes (SPM world-space):
% Note: Yields identical results as loading & saving with NIFTImatlab.
NormSource = align_ijk2xyz(NormSource);


% Save NormSource:
CheckSavePath(OutpathNormSource, 'NormaliseSPM');
save(OutpathNormSource, 'NormSource');
