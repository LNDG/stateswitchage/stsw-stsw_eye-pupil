%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVANCED SETTINGS FOR SOURCE ANALYSIS: %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This settings file will be read by the BuilderGUI.
% - Settings here will be loaded into the Builder .mat and applied to analysis.
%
% Usage:
%  a)  Settings under "DO NOT EDIT" are handled by [MEG]PLS. Do not alter these.
%  b)  Change other values as needed. See "help" command for FieldTrip section.
%  c)  Optional settings can be uncommented and specified.
%
function [cfgSource, cfgInterpSrc, cfgWriteSrc, cfgSrcAvg] = Settings_SourceAnalysis(InputFTcfg)

% DO NOT EDIT:
cfgSource = InputFTcfg.Source;



%--- "FT_SOURCEANALYSIS" SETTINGS: ---%
%-------------------------------------%

% DO NOT EDIT:
cfgSource.method                          = InputFTcfg.Source.method;
cfgSource.vol                             = [];
cfgSource.grid                            = [];    % Precomputed leadfield & filter used.
cfgSource.(cfgSource.method).keepfilter   = 'no';  % Turned on for source-filter calc.
cfgSource.(cfgSource.method).keepmom      = 'yes';
cfgSource.(cfgSource.method).keepcsd      = 'yes';
cfgSource.(cfgSource.method).projectnoise = 'yes';
cfgSource.(cfgSource.method).feedback     = 'text';
cfgSource.channel                         = 'MEG';
if ismember(InputFTcfg.Source.method, {'lcmv', 'dics', 'pcc'})
    cfgSource.(cfgSource.method).fixedori = 'no';  % Keep as vector output
end
if strcmp(InputFTcfg.Source.method, 'sam')
    cfgSource.(cfgSource.method).fixedori = 'spinning';  % Options: spinning, robert, gareth
end


% GENERAL SETTINGS:
cfgSource.rawtrial      = 'no';  % Construct filter from single trials, apply to single trials.
cfgSource.keeptrials    = 'no';  % If 'yes', then need rawtrial to be 'yes' as well.
cfgSource.keepleadfield = 'no';  % Note: rawtrial & keeptrial are disabled for filter calculation


% OPTIONAL SETTINGS:
%  cfgSource.(cfgSource.method).lambda = number, percentage-string (Ex: '5%'), or empty

%  cfgSource.frequency = single number (in Hz)
%  cfgSource.latency   = single number in seconds, for time-frequency analysis

%  cfgSource.refchan   = reference channel label (for coherence)
%  cfgSource.refdip    = reference dipole location (for coherence)
%  cfgSource.supchan   = suppressed channel label(s)
%  cfgSource.supdip    = suppressed dipole location(s)


% OPTIONAL STATISTICS:
%  cfgSource.jackknife    = 'no' or 'yes'   jackknife resampling of trials
%  cfgSource.pseudovalue  = 'no' or 'yes'   pseudovalue resampling of trials
%  cfgSource.bootstrap    = 'no' or 'yes'   bootstrap resampling of trials
%  cfgSource.numbootstrap = number of bootstrap replications (e.g. number of original trials)

%  cfgSource.randomization    = 'no' or 'yes'
%  cfgSource.permutation      = 'no' or 'yes'
%  cfgSource.numrandomization = number, e.g. 500
%  cfgSource.numpermutation   = number, e.g. 500 or 'all'


% MANUAL GRAD. DEFINITION (NOT REQUIRED FOR NORMAL USE):
%  cfgSource.elec          = structure with electrode positions, see FT_DATATYPE_SENS
%  cfgSource.grad          = structure with gradiometer definition, see FT_DATATYPE_SENS
%  cfgSource.elecfile      = name of file containing the electrode positions, see FT_READ_SENS
%  cfgSource.gradfile      = name of file containing the gradiometer definition, see FT_READ_SENS

% SETTINGS IF NO PRECOMPUTED LEADFIELD (NOT REQUIRED FOR NORMAL USE):
%  cfgSource.reducerank = 2;
%  cfgSource.normalize  = 'no';



%--- "FT_SOURCEINTERPOLATE" SETTINGS FOR SOURCE: ---%
%---------------------------------------------------%

% Note: This section determines how non-normalised source files are
%       interpolated and what parameter is written out into NIFTI format.

% GENERAL SETTINGS:
cfgInterpSrc.downsample   = 1;
cfgInterpSrc.interpmethod = 'nearest';
cfgInterpSrc.parameter    = 'avg.OutputStat';  % String or cell-array of field(s) to be interpolated.
%                                                If empty, will set to interpolate all volume fields.



%--- "FT_VOLUMEWRITE" SETTINGS FOR INTERPOLATED SOURCES: ---%
%-----------------------------------------------------------%

% Note: This section determines how non-normalised source files
%       are written out into NIFTI format.

% Warning: This section controls what parameter is written out into NIFTI for RAW sources.
%          For write settings to NORMALISED sources, see "Settings_SourceNormalise".

% DO NOT EDIT:
cfgWriteSrc.filename   = [];          % Handled by pipeline.
cfgWriteSrc.filetype   = 'nifti';     % Currently hardcoded for NIFTI.

% GENERAL SETTINGS:
cfgWriteSrc.downsample = 1;
cfgWriteSrc.parameter  = cfgInterpSrc.parameter;
cfgWriteSrc.datatype   = 'double';
cfgWriteSrc.scaling    = 'no';

% OPTIONAL SETTINGS:
%  cfgWriteSrc.fiducial.nas = [x y z] position of nasion
%  cfgWriteSrc.fiducial.lpa = [x y z] position of LPA
%  cfgWriteSrc.fiducial.rpa = [x y z] position of RPA
%  cfgWriteSrc.markfiducial = 'yes' or 'no', mark the fiducials
%  cfgWriteSrc.markorigin   = 'yes' or 'no', mark the origin
%  cfgWriteSrc.markcorner   = 'yes' or 'no', mark the first corner of the volume



%--- "FT_SOURCEGRANDAVERAGE" SETTINGS: ---%
%-----------------------------------------%

% Note: This section controls group-averaging settings in the pipeline, and determines
%       what parameters are group-averaged after normalisation.

cfgSrcAvg.parameter      = cfgInterpSrc.parameter;
cfgSrcAvg.keepindividual = 'no';
